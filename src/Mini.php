<?php

namespace Ox3a\VkApi;

/**
 * Класс для работы с VK-API
 * @property string $client_id
 * @property string $secret_key
 * @property string $userid
 * @property string $token
 * @property string $url
 * @property string $version
 */
class Mini {

    /**
     * Список ошибок
     * @var string[] 
     */
    protected $_errors = [];

    /**
     * Свойства подклюения
     * @var string[]
     */
    protected $_properties = [
        'client_id'  => '',
        'secret_key' => '',
        'userid'     => '',
        'token'      => '',
        'url'        => '',
        'version'    => '3.0'
    ];

    /**
     * Кеш для существующих списков методов вк
     * @var boolean[]
     */
    protected static $_methods = [];

    /**
     * cUrl
     * @var resource
     */
    protected $_curl;

    /**
     * Конструктор
     * @param string[] $data
     */
    public function __construct($data) {
        $this->setProperties($data);
    }

    /**
     * Установка свойств
     * @param string[] $data
     */
    public function setProperties($data) {
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $this->_properties)) {
                $this->_properties[$key] = $value;
            }
        }
    }

    /**
     * Геттер
     * @param type $key
     * @return type
     */
    public function __get($key) {
        return isset($this->_properties[$key]) ? $this->_properties[$key] : null;
    }

    /**
     * Иссетер
     * @param type $key
     * @return type
     */
    public function __isset($key) {
        return array_key_exists($key, $this->_properties);
    }

    /**
     * Выполнить запрос
     * @param string $method
     * @param array $data
     * @return type
     */
    public function query($method, array $data) {
        $this->_errors = [];
        sleep(1);
        echo "$method\n";
        if (($classMethod   = $this->_methodExists($method))) {//isset($this->_methodAssocc[$method])) {
            return $this->{$classMethod}($data);
        } else {
            return $this->_getResponse($method, $data);
        }
    }

    /**
     * Получить ошибки
     * @return array
     */
    public function getErrors() {
        return $this->_errors;
    }

    /**
     * Получить спецметод для метода вк
     * @param string $vkMethod
     * @return string|false
     */
    protected function _methodExists($vkMethod) {
        if (!key_exists($vkMethod, self::$_methods)) {
            $method = '_' . str_replace('.', '', $vkMethod);

            if (method_exists($this, $method)) {
                self::$_methods[$vkMethod] = $method;
            } else {
                self::$_methods[$vkMethod] = false;
            }
        }
        return self::$_methods[$vkMethod];
    }

    /**
     * Получить ответ
     * @param string $method
     * @param array $data
     * @return false|stdClass
     */
    protected function _getResponse($method, array $data) {
        $data['access_token'] = $this->token;
        $data['v']            = $this->version;

        $jsonText = $this->postQuery($this->url . $method, $data);
        $response = json_decode($jsonText);

        if (JSON_ERROR_NONE != json_last_error()) {
            $this->_errors[] = 'JSON: #' . json_last_error();
            return false;
        }

        if (isset($response->error)) {
            $this->_errors[] = $response->error->error_msg;
            return false;
        } elseif (isset($response->response)) {
            return $response->response;
        }

        $this->_errors[] = "Херня какая-то: \n" . print_r($response, true);
        return false;
    }

    /**
     * Гет запрос
     * @param string $url
     * @return string
     */
    public function getQuery($url) {
        $ch = $this->_getCurl();

        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_URL, $url);

        return curl_exec($ch);
    }

    /**
     * Отправить пост запрос
     * @param string $url
     * @param array $data
     * @param array $files
     * @return string
     */
    public function postQuery($url, array $data = [], array $files = null) {
        $ch = $this->_getCurl();

        if ($files) {
            foreach ($files as $field => $file) {
                // php 5.6+
                if (function_exists('curl_file_create')) {
                    $cFile = curl_file_create($file);
                } else { // 
                    $cFile = '@' . realpath($file);
                }
                $data[$field] = $cFile;
            }
        }

        curl_setopt($ch, CURLOPT_POST, true);
        if ($data) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, []);
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        return curl_exec($ch);
    }

    /**
     * Получить curl
     * @return cURLHandler
     */
    protected function _getCurl() {
        if (!$this->_curl) {
            $this->_curl = curl_init();
            curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->_curl, CURLOPT_HEADER, false);
            curl_setopt($this->_curl, CURLOPT_VERBOSE, false);
        }

        return $this->_curl;
    }

    /**
     * Загрузка фото в альбом
     * @param array $data
     * @return false|response
     */
    protected function _photosAlbum($data) {
        $response = $this->query('photos.getUploadServer', [
            'album_id' => $data['album_id'],
            'group_id' => $data['group_id']
        ]);

        if ($response) {
            $return  = $this->postQuery($response->upload_url, array('file1' => '@' . $data['photo']));
            $request = json_decode($return, true);

            if ($request['photos_list']) {
                $request['caption']  = $data['caption'];
                $request['album_id'] = $data['album_id'];
                $request['group_id'] = $data['group_id'];

                return $this->_getResponse('photos.save', $request);
            } else {
                $this->_errors[] = 'Не загрузилось';
            }
        }
        return false;
    }

    /**
     * Прикрепление фото к стене
     * @param array $data
     * @return false|response
     */
    protected function _photosWall(array $data) {
        $response = $this->_getResponse('photos.getWallUploadServer', array('group_id' => $data['group_id']));
        if ($response) {

            $return  = $this->postQuery($response->upload_url, [], array('photo' => $data['photo']));
            $request = json_decode($return, true);
            if ($request['photo']) {
                $request['group_id'] = $data['group_id'];
                return $this->_getResponse('photos.saveWallPhoto', $request);
            } else {
                $this->_errors[] = 'Не загрузилось';
            }
        }
        return false;
    }

}
